# Ubuntu on USB

### How to install a complete Linux system on a USB stick?

I just learned an amazing trick. You can have a complete Ubuntu system on a thumb drive. 
I don't mean putting a Linux install media on a USB drive. 
I don't mean even trying out the Ubuntu "live disk".
I mean, you can install a real complete Ubuntu system on a USB drive.

It's a bit "messy" to do this unfortunately, but once you figure out how to do it it's totally worth it.
Imagine how many linux "machines" you can have.
All you need is one USB drive per "machine".
(It's sort of a "virtual machine", not unlike a vm, since it requires a host system, but nonetheless it's a real operating system.)

The answer "Installing Ubuntu to a removable USB drive with Virtual Box" in the reference I cite below
explains how to do this, and I'll not reproduce the content here.
But it suffices to say that there are many moving parts. 
You'll have to figure out how to boot from a USB stick on your "host computer" (e.g., a Windows laptop).
You'll have to learn how to use VirtualBox if you haven't used it before. You'll need to figure out how to use USB in VirtualBox.
Etc. etc. These are all trivial tasks, but if you'll have to figure it out if you haven't done any of these things before.

I must say though, it's completely worth it.

Note: Linux, including Ubuntu distro, used to be know for its "small footprint".
The current Ubuntu desktop 16.04 LTS CANNOT be installed on 8G USB flash drive. What? Yes. That is true.
Sadly, Ubuntu is getting bloated. (Obviously, there are other distros known for their small size, 
but it's always a safe bet to use a more widely used distro for this kind of "experiment".)
Anyways, you'll need a USB memory bigger than 8G, which means you will need 16G or bigger.



### What is a computer's "brain"?

Often the CPU of a computer is compared with the human brain.
While I was experimenting with "Linux on USB",
I realized that that is not entirely correct.
Human brains do many things.
It does information processing, among other things.
It also store information. It's more like a persistent storage, i.e., a hard disk of a computer.

Now, if you think about it, the "identity" of a human being comes from the hard disk nature of the brain, not from the CPU nature of the brain.

The USB memory in "Linux on USB" is really a hard disk.
It does not have a processing power. It can "borrow" the processing power from a host computer, in fact, from any computer.

If you think about it,
the computer's brain is the "dumb" hard disk, not the fancy CPU.


### References

* [Create a bootable USB stick on Windows](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-windows)

* [Installation/FromUSBStick](https://help.ubuntu.com/community/Installation/FromUSBStick)

* [LiveCD/Persistence](https://help.ubuntu.com/community/LiveCD/Persistence)

* [LiveUsbPendrivePersistent](https://wiki.ubuntu.com/LiveUsbPendrivePersistent)

* [How do I install Ubuntu to a USB key?](https://askubuntu.com/questions/16988/how-do-i-install-ubuntu-to-a-usb-key-without-using-startup-disk-creator)








